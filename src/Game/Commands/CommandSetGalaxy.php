<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Helpers\Random;
use BinaryStudioAcademy\Game\Universe\Galaxy;

class CommandSetGalaxy implements \BinaryStudioAcademy\Game\Contracts\Commands\Command
{
    private $writer;
    private $galaxy;
    private $target;
    private $random;

    public function __construct(string $target, Writer $writer, Galaxy $galaxy, $random)
    {
        $this->writer = $writer;
        $this->galaxy = $galaxy;
        $this->target = $target;
        $this->random = $random;
    }

    public function execute(): void
    {
        if (array_key_exists($this->target, Galaxy::GALAXIES)) {
            $this->galaxy->galaxyName = $this->target;
            if ($this->target == 'home') {
                $this->galaxy->galaxyEnemyShip = null;
                $this->writer->writeln('Galaxy: Home Galaxy.');
                return;
            }
            $this->galaxy->createEnemyShip($this->random);
            $this->writer->writeln('Galaxy: ' . $this->galaxy->showFullName() . '.');
            $this->writer->writeln('You see a ' . $this->galaxy->galaxyEnemyShip->getFullName() . ': ');
            $this->writer->writeln('strength: ' . $this->galaxy->galaxyEnemyShip->strength);
            $this->writer->writeln('armor: ' . $this->galaxy->galaxyEnemyShip->armor);
            $this->writer->writeln('luck: ' . $this->galaxy->galaxyEnemyShip->luck);
            $this->writer->writeln('health: ' . $this->galaxy->galaxyEnemyShip->health);
        } else {
            $this->writer->writeln("Nah. No specified galaxy found.");
        }
    }
}
