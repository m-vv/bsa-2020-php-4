<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Universe\Galaxy;
use BinaryStudioAcademy\Game\Universe\UserShip;

class CommandGrab implements \BinaryStudioAcademy\Game\Contracts\Commands\Command
{
    private $writer;
    private $ship;
    private $galaxy;

    public function __construct(Writer $writer, UserShip $ship, Galaxy $galaxy)
    {
        $this->writer = $writer;
        $this->ship = $ship;
        $this->galaxy = $galaxy;
    }

    public function execute(): void
    {
        if ($this->galaxy->galaxyName == 'home') {
            $this->writer->writeln('Hah? You don\'t want to grab any staff at Home Galaxy. Believe me.');
            return;
        }
        if ($this->galaxy->galaxyEnemyShip->health > 0) {
            $this->writer->writeln('LoL. Unable to grab goods. Try to destroy enemy spaceship first.');
            return;
        }
        if (is_null($this->galaxy->galaxyEnemyShip)) {
            $this->writer->writeln('LoL. Unable to grab goods.  These galaxy does not have spaceships except you.');
            return;
        }
        if ($this->galaxy->galaxyEnemyShip->classShip == 'patrol') {
            array_push($this->ship->hold, 2);
            $this->writer->writeln('You got 🔋.');
            $this->galaxy->galaxyEnemyShip = null;
            return;
        }
        if ($this->galaxy->galaxyEnemyShip->classShip == 'battle') {
            array_push($this->ship->hold, 2, 1);
            $this->writer->writeln('You got 🔋 🔮.');
            $this->galaxy->galaxyEnemyShip = null;
            return;
        }
    }
}
