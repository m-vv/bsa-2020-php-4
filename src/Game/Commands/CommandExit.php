<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class CommandExit implements \BinaryStudioAcademy\Game\Contracts\Commands\Command
{
    private $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    public function execute(): void
    {
        $this->writer->writeln('Galaxy Warriors over!');
        exit;
    }
}
