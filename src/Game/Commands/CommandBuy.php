<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Universe\Galaxy;
use BinaryStudioAcademy\Game\Universe\UserShip;

class CommandBuy implements \BinaryStudioAcademy\Game\Contracts\Commands\Command
{
    private $writer;
    private $ship;
    private $galaxy;
    private $param;

    public function __construct(Writer $writer, UserShip $ship, Galaxy $galaxy, string $param)
    {
        $this->writer = $writer;
        $this->ship = $ship;
        $this->galaxy = $galaxy;
        $this->param = $param;
    }
    public function execute(): void
    {
        if ($this->galaxy->galaxyName != 'home') {
            $this->writer->writeln('You can make exchange only in home galaxy. Please change galaxy!');
            return;
        }
        if (count(array_keys($this->ship->hold, 2)) > 0) {
            unset($this->ship->hold[array_search(2, $this->ship->hold)]);
            switch ($this->param) {
                case 'strength':
                    $this->ship->strength += 1;
                    $this->writer->writeln("You've got upgraded skill: {$this->param}. The level is {$this->ship->strength} now.");
                    break;
                case 'armor':
                    $this->ship->armor += 1;
                    $this->writer->writeln("You've got upgraded skill: {$this->param}. The level is {$this->ship->armor} now.");
                    break;
                case 'reactor':
                    array_push($this->ship->hold, 1);
                    $this->writer->writeln("You've bought a magnet reactor. You have {count(array_keys($this->ship->hold, 1)} reactor(s) now.");
                    break;
                default:
                    $this->writer->writeln("Unknown  subject of exchange");
            }
        } else {
            $this->writer->writeln("You don't have crystals nothing changed");
        }
    }
}
