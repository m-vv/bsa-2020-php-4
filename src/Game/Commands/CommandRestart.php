<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Universe\Galaxy;
use BinaryStudioAcademy\Game\Universe\UserShip;

class CommandRestart implements \BinaryStudioAcademy\Game\Contracts\Commands\Command
{
    private $ship;
    private $galaxy;

    public function __construct(UserShip $ship, Galaxy $galaxy)
    {
        $this->ship = $ship;
        $this->galaxy = $galaxy;
    }
    public function execute(): void
    {
        $this->ship->reset();
        $this->galaxy->galaxyName = 'home';
        $this->galaxy->galaxyEnemyShip = null;
    }
}
