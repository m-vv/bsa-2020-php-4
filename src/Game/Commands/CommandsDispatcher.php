<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Helpers\Random;
use BinaryStudioAcademy\Game\Universe\Galaxy;
use BinaryStudioAcademy\Game\Universe\UserShip;

class CommandsDispatcher
{
    private $writer;
    private $invoker;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
        $this->invoker = new Invoker();
    }

    public function run(string $command_param, UserShip $ship, Galaxy $galaxy, $random)
    {
        $t = explode(' ', $command_param);
        if (count($t) <= 1) {
            $command = $t[0];
            $param = '';
        } else {
            $command = $t[0];
            $param = $t[1];
        }
        switch ($command) {
            case 'exit':
                $this->invoker->setCommand(new CommandExit($this->writer));
                $this->invoker->run();
                break;
            case 'stats':
                $this->invoker->setCommand(new CommandStats($this->writer, $ship));
                $this->invoker->run();
                break;
            case 'help':
                $this->invoker->setCommand(new CommandHelp($this->writer));
                $this->invoker->run();
                break;
            case 'whereami':
                $this->invoker->setCommand(new CommandWhereami($this->writer, $galaxy));
                $this->invoker->run();
                break;
            case 'set-galaxy':
                $this->invoker->setCommand(new CommandSetGalaxy($param, $this->writer, $galaxy, $random));
                $this->invoker->run();
                break;
            case 'restart':
                $this->invoker->setCommand(new CommandRestart($ship, $galaxy));
                $this->invoker->run();
                break;
            case 'grab':
                $this->invoker->setCommand(new CommandGrab($this->writer, $ship, $galaxy));
                $this->invoker->run();
                break;
            case 'attack':
                $this->invoker->setCommand(new CommandAttack($this->writer, $ship, $galaxy, $random));
                $this->invoker->run();
                break;
            case 'apply-reactor':
                $this->invoker->setCommand(new CommandApplyReactor($this->writer, $ship, $galaxy));
                $this->invoker->run();
                break;
            case 'buy':
                $this->invoker->setCommand(new CommandBuy($this->writer, $ship, $galaxy, $param));
                $this->invoker->run();
                break;
            default:
                $this->writer->writeln("Command '$command' not found");
        }
    }
}
