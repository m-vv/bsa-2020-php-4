<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Universe\Galaxy;
use BinaryStudioAcademy\Game\Universe\UserShip;

class CommandApplyReactor implements \BinaryStudioAcademy\Game\Contracts\Commands\Command
{
    private $writer;
    private $ship;
    private $galaxy;

    public function __construct(Writer $writer, UserShip $ship, Galaxy $galaxy)
    {
        $this->writer = $writer;
        $this->ship = $ship;
        $this->galaxy = $galaxy;
    }
    public function execute(): void
    {
        if (count(array_keys($this->ship->hold, 1)) > 0) {
            $this->ship->health += 20;
            unset($this->ship->hold[array_search(1, $this->ship->hold)]);
            $this->writer->writeln("Magnet reactor have been applied. Current spaceship health level is {$this->ship->health}");
        } else {
            $this->writer->writeln("Magnet reactor have not been applied. Current spaceship health level is {$this->ship->health}");
        }
    }
}
