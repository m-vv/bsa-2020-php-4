<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Universe\UserShip;

class CommandStats implements \BinaryStudioAcademy\Game\Contracts\Commands\Command
{
    private $writer;
    private $ship;

    public function __construct(Writer $writer, UserShip $ship)
    {
        $this->writer = $writer;
        $this->ship = $ship;
    }
    public function execute(): void
    {
        $this->writer->writeln('Spaceship stats:');
        $this->writer->writeln('strength: ' . $this->ship->strength);
        $this->writer->writeln('armor: ' . $this->ship->armor);
        $this->writer->writeln('luck: ' . $this->ship->luck);
        $this->writer->writeln('health: ' . $this->ship->health);
        $this->writer->writeln('hold: ' . $this->ship->getHold());
    }
}
