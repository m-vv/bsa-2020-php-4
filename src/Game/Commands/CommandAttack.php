<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Helpers\Random;
use BinaryStudioAcademy\Game\Universe\Galaxy;
use BinaryStudioAcademy\Game\Universe\UserShip;

class CommandAttack implements \BinaryStudioAcademy\Game\Contracts\Commands\Command
{
    private $writer;
    private $ship;
    private $galaxy;
    private $mathHelper;
    private $random;

    public function __construct(Writer $writer, UserShip $ship, Galaxy $galaxy, $random)
    {
        $this->writer = $writer;
        $this->ship = $ship;
        $this->galaxy = $galaxy;
        $this->mathHelper = new Math();
        $this->random = $random;
    }
    public function execute(): void
    {
        if ($this->galaxy->galaxyName == 'home') {
            $this->writer->writeln('Calm down! No enemy spaceships detected. No one to fight with.');
            return;
        }
        if ($this->galaxy->galaxyEnemyShip->health <= 0) {
            $this->writer->writeln('Calm down! No enemy spaceships detected. No one to fight with.');
            return;
        }

        $userDamage = $this->mathHelper->damage($this->ship->strength, $this->galaxy->galaxyEnemyShip->armor);
        $userLuck = $this->mathHelper->luck($this->random, $this->galaxy->galaxyEnemyShip->luck);
        $userDamage = ($userLuck)  ? $userDamage : 0;
        $this->galaxy->galaxyEnemyShip->health -=  $userDamage;
        if ($this->galaxy->galaxyEnemyShip->health < 0) {
            if ($this->galaxy->galaxyEnemyShip->classShip == 'executor') {
                $this->writer->writeln('🎉🎉🎉 Congratulations 🎉🎉🎉');
                $this->writer->writeln('🎉🎉🎉 You are winner! 🎉🎉🎉');
                $invoke = new Invoker();
                $invoke ->setCommand(new CommandExit($this->writer));
                $invoke -> run();
                return;
            } else {
                $this->writer->writeln("{$this->galaxy->galaxyEnemyShip->getFullName()} is totally destroyed. Hurry up! There is could be something useful to grab.");
                return;
            }
        } else {
            $this->writer->writeln("{$this->galaxy->galaxyEnemyShip->getFullName()} has damaged on: {$userDamage} points.");
            $this->writer->writeln("health: {$this->galaxy->galaxyEnemyShip->health }");
        }
        $enemyDamage = $this->mathHelper->damage($this->galaxy->galaxyEnemyShip->strength, $this->ship->armor);
        $userLuck = $this->mathHelper->luck($this->random, $this->ship->luck);
        $enemyDamage = ($userLuck)  ? $enemyDamage : 0;
        $this->ship->health -=  $enemyDamage;
        if ($this->ship->health < 0) {
            $this->writer->writeln("Your spaceship got significant damages and eventually got exploded.");
            $this->writer->writeln("You have to start from Home Galaxy.");
            $invoke = new Invoker();
            $invoke ->setCommand(new CommandRestart($this->ship, $this->galaxy));
            $invoke -> run();
            return;
        } else {
            $this->writer->writeln("{$this->galaxy->galaxyEnemyShip->getFullName()} damaged your spaceship on: {$enemyDamage} points.");
            $this->writer->writeln("health: {$this->ship->health}");
        }
    }
}
