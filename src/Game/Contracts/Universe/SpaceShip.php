<?php

namespace BinaryStudioAcademy\Game\Contracts\Universe;

interface SpaceShip
{
    public function getHold(): string;
}
