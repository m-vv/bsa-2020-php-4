<?php

namespace BinaryStudioAcademy\Game\Contracts\Universe;

interface SpaceShipFactory
{
    public function createSpaceShip(): SpaceShip;
}
