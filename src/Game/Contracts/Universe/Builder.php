<?php

namespace BinaryStudioAcademy\Game\Contracts\Universe;

interface Builder
{
    public function createSpaceShip();
    public function setStrength(): void;
    public function setArmor(): void;
    public function setLuck(): void;
    public function setHealth(): void;
    public function setHold(): void;
    public function setClass(): void;
    public function getSpaceShip(): Spaceship;
}
