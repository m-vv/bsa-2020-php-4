<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Commands\CommandsDispatcher;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Universe\Galaxy;
use BinaryStudioAcademy\Game\Universe\UserShipFactory;

class Game
{
    private $random;
    private $userShip;
    private $currentGalaxy;
    private $dispatcher;

    public function __construct($random)
    {
        $this->random = $random;
        $userShipFactory = new UserShipFactory();
        $this->userShip = $userShipFactory->createSpaceShip();
        $this->currentGalaxy = new Galaxy();
    }

    public function start(Reader $reader, Writer $writer)
    {
        $writer->writeln('Welcome to  game "Galaxy Warriors".');
        $this->dispatcher = new CommandsDispatcher($writer);
        $writer->writeln('Adventure has begun. Wish you good luck!');
        while (true) {
            $writer->writeln('Enter command: ');
            $input = trim($reader->read());
            $this->dispatcher->run($input, $this->userShip, $this->currentGalaxy, $this->random);
        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        $this->dispatcher = new CommandsDispatcher($writer);
        if (is_null($this->userShip)) {
            $userShipFactory = new UserShipFactory();
            $this->userShip = $userShipFactory->createSpaceShip();
        }
        if (is_null($this->currentGalaxy)) {
            $this->currentGalaxy = new Galaxy();
        }
        $input = trim($reader->read());
        $this->dispatcher->run($input, $this->userShip, $this->currentGalaxy, $this->random);
    }
}
