<?php

namespace BinaryStudioAcademy\Game\Universe;

use BinaryStudioAcademy\Game\Contracts\Universe\Spaceship;

class UserShipBuilder implements \BinaryStudioAcademy\Game\Contracts\Universe\Builder
{
    private $ship;

    public function createSpaceShip()
    {
        $this->ship = new UserShip();
    }

    public function setStrength(): void
    {
        $this->ship ->strength = 5;
    }

    public function setArmor(): void
    {
        $this->ship->armor = 5;
    }

    public function setLuck(): void
    {
        $this->ship->luck = 5;
    }

    public function setHealth(): void
    {
        $this->ship->health = 100;
    }

    public function setHold(): void
    {
        $this->ship->hold = [0,0,0];
    }

    public function setClass(): void
    {
        $this->ship->classShip = 'player';
    }

    public function getSpaceShip(): Spaceship
    {
        return $this->ship;
    }
}
