<?php

namespace BinaryStudioAcademy\Game\Universe;

use BinaryStudioAcademy\Game\Contracts\Universe\SpaceShip;
use BinaryStudioAcademy\Game\Helpers\Random;

class EnemyShipFactory implements \BinaryStudioAcademy\Game\Contracts\Universe\SpaceShipFactory
{
    private $classShip;
    private $random;

    public function __construct(string $classShip, $random)
    {
        $this->classShip = $classShip;
        $this->random = $random;
    }

    public function createSpaceShip(): SpaceShip
    {
        $enemyShipBuilder = new EnemyShipBuilder($this->classShip, $this->random);
        return  (new Director())->build($enemyShipBuilder);
    }
}
