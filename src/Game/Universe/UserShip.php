<?php

namespace BinaryStudioAcademy\Game\Universe;

class UserShip implements \BinaryStudioAcademy\Game\Contracts\Universe\SpaceShip
{
    public $strength;
    public $armor;
    public $luck;
    public $health;
    public $hold ;
    public $classShip;

    public function getHold(): string
    {
 // 0 empty 1  power 2 cristal
        $t = '[ ';
        foreach ($this->hold as $item) {
            switch ($item) {
                case 0:
                    $t .= '_ ';
                    break;
                case 1:
                    $t .= '🔋';
                    break;
                case 2:
                    $t .= '🔮';
                    break;
            }
        }
        $t .= ']';
        return $t;
    }

    public function reset()
    {
        $this->strength = 5;
        $this->armor  =  5;
        $this->luck = 5;
        $this->health = 100;
        $this ->hold = [0,0,0];
    }
}
