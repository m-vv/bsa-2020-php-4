<?php

namespace BinaryStudioAcademy\Game\Universe;

use BinaryStudioAcademy\Game\Contracts\Universe\SpaceShip;

class UserShipFactory implements \BinaryStudioAcademy\Game\Contracts\Universe\SpaceShipFactory
{

    public function createSpaceShip(): SpaceShip
    {
        $userShipBuilder = new UserShipBuilder();
        return  (new Director())->build($userShipBuilder);
    }
}
