<?php

namespace BinaryStudioAcademy\Game\Universe;

class EnemyShip implements \BinaryStudioAcademy\Game\Contracts\Universe\SpaceShip
{
    public $strength;
    public $armor;
    public $luck;
    public $health;
    public $hold ;
    public $classShip;

    public const SPACESHIPS = [
        'patrol' => [
            'name' => 'Patrol Spaceship',
            'stats' => [
                'strength' => 4,
                'armor' => 4,
                'luck' => 2,
                'health' => 100,
            ]
        ],
        'battle' => [
            'name' => 'Battle Spaceship',
            'stats' => [
                'strength' => 8,
                'armor' => 7,
                'luck' => 6,
                'health' => 100,
            ]
        ],
        'executor' => [
            'name' => 'Executor',
            'stats' => [
                'strength' => 10,
                'armor' => 10,
                'luck' => 10,
                'health' => 100,
            ]
        ]
    ];

    public function __construct(string $classShip)
    {
        $this->classShip = $classShip;
    }


    public function getHold(): string
    {
        // 0 empty 1  power 2 cristal
        $t = '[ ';
        foreach ($this->hold as $item) {
            switch ($item) {
                case 0:
                    $t .= '_ ';
                    break;
                case 1:
                    $t .= '🔋';
                    break;
                case 2:
                    $t .= '🔮';
                    break;
            }
        }
        $t .= ']';
        return $t;
    }

    public function getFullName()
    {
        return self::SPACESHIPS[$this->classShip]['name'];
    }
}
