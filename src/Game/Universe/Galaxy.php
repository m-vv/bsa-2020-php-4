<?php

namespace BinaryStudioAcademy\Game\Universe;

use BinaryStudioAcademy\Game\Helpers\Random;

class Galaxy
{
    public $galaxyName;
    public $galaxyEnemyShip;
    public const GALAXIES = [
        'home' => [
            'galaxy' => 'Home Galaxy',
            'spaceship' => 'player'
        ],
        'andromeda' => [
            'galaxy' => 'Andromeda',
            'spaceship' => 'patrol'
        ],
        'pegasus' => [
            'galaxy' => 'Pegasus',
            'spaceship' => 'patrol'
        ],
        'spiral' => [
            'galaxy' => 'Spiral',
            'spaceship' => 'patrol'
        ],
        'shiar' => [
            'galaxy' => 'Shiar',
            'spaceship' => 'battle'
        ],
        'xeno' => [
            'galaxy' => 'Xeno',
            'spaceship' => 'battle'
        ],
        'isop' => [
            'galaxy' => 'Isop',
            'spaceship' => 'executor'
        ]
    ];

    public function __construct()
    {
        $this->galaxyName = 'home';
    }

    public function showFullName(): string
    {
        return self::GALAXIES[$this->galaxyName]['galaxy'];
    }

    public function createEnemyShip($random)
    {
        $enemyShipFactory = new EnemyShipFactory(self::GALAXIES[$this->galaxyName]['spaceship'], $random);
        $this->galaxyEnemyShip = $enemyShipFactory->createSpaceShip();
    }
}
