<?php

namespace BinaryStudioAcademy\Game\Universe;

use BinaryStudioAcademy\Game\Contracts\Universe\Builder;

class Director
{
    public function build(Builder $builder)
    {
        $builder->createSpaceShip();
        $builder->setClass();
        $builder->setStrength();
        $builder->setArmor();
        $builder->setLuck();
        $builder->setHealth();
        $builder->setHold();
        return $builder->getSpaceShip();
    }
}
