<?php

namespace BinaryStudioAcademy\Game\Universe;

use BinaryStudioAcademy\Game\Contracts\Universe\Spaceship;
use BinaryStudioAcademy\Game\Helpers\Random;

class EnemyShipBuilder implements \BinaryStudioAcademy\Game\Contracts\Universe\Builder
{
    private $classShip;
    private $ship;
    private $random;

    public function __construct(string $classShip, $random)
    {
        $this->classShip = $classShip;
        $this->random = $random;
    }

    public function createSpaceShip()
    {
        $this->ship = new EnemyShip($this->classShip);
    }

    public function setStrength(): void
    {
        switch ($this->classShip) {
            case 'patrol':
                $this->ship ->strength = intval(3 + $this->random->get() * (4.1 - 3));
                break;
            case 'battle':
                $this->ship ->strength = intval(5 +  $this->random->get() * (8.1 - 5));
                break;
            case 'executor':
                $this->ship ->strength = 10;
                break;
        }
    }

    public function setArmor(): void
    {
        switch ($this->classShip) {
            case 'patrol':
                $this->ship ->armor = intval(2 +  $this->random->get() * (4.1 - 2));
                break;
            case 'battle':
                $this->ship ->armor = intval(6 +  $this->random->get() * (7.1 - 6));
                break;
            case 'executor':
                $this->ship ->armor = 10;
                break;
        }
    }

    public function setLuck(): void
    {
        switch ($this->classShip) {
            case 'patrol':
                $this->ship ->luck = intval(1 +  $this->random->get() * (2.1 - 1));
                break;
            case 'battle':
                $this->ship ->luck = intval(3 +  $this->random->get() * (6.1 - 3));
                break;
            case 'executor':
                $this->ship ->luck = 10;
                break;
        }
    }

    public function setHealth(): void
    {
        $this->ship->health = 100;
    }

    public function setHold(): void
    {
        switch ($this->classShip) {
            case 'patrol':
                $this->ship ->hold = [2, 0, 0];
                break;
            case 'battle':
                $this->ship ->hold = [2, 1, 0];
                break;
            case 'executor':
                $this->ship ->hold = [2, 1, 1];
                break;
        }
    }

    public function setClass(): void
    {
        $this->ship ->classShip = $this->classShip;
    }

    public function getSpaceShip(): Spaceship
    {
        return $this->ship;
    }
}
